﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleShooter : MonoBehaviour
{
    public GameObject bubble;
    public List<GameObject> bubblesStack;
    public GameObject bubblePrefab;
    public int bubblesInitialAmount;
    public static BubbleShooter instance;

    private void Start()
    {
        if(instance)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        InitializeBubbleStack(bubblesInitialAmount);
        GetNewBubble();

    }

    private void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            Shoot();
            GetNewBubble();
        }

    }

    public void Shoot()
    {
        bubble.GetComponent<Shootable>().Shoot();
    }


    public void GetNewBubble()
    {
        if (bubblesStack.Count > 0)
        {
            GameManager.Instance.ShotsRemaining = bubblesStack.Count;
            bubble = bubblesStack[0];
            bubble.SetActive(true);
            bubblesStack.RemoveAt(0);
        }
        else
        {
            //lose
        }
    }
    public void InitializeBubbleStack(int n)
    {
        GameManager.Instance.ShotsRemaining = bubblesInitialAmount;
        for(int i=0;i<n;i++)
        {
            GameObject temp = Instantiate(bubblePrefab);
            temp.transform.parent = transform;
            temp.GetComponent<Collider2D>().enabled = false;
            temp.transform.position = transform.position;
            temp.SetActive(false);
            bubblesStack.Add(temp);
        }
    }
}
