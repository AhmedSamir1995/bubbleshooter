﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParablolicEq : MonoBehaviour
{
    public LineRenderer lineRenderer;

    public float a, b, c;

    public float startX, endX, step;
    [Range(.001f,1f)]
    public float scale;
    [Range(0,360f)]
    public float theta;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        #region oldParabolic
        //Vector3 point;
        //List<Vector3> curve = new List<Vector3>();
        ////eq y= ax2+bx+c
        //if(step>0)
        //    for (float i = startX; i <= endX; i += step)
        //    {

        //        point.y = (a * i * i + b * i + c);
        //        point.x = i;

        //        //point.x = point.x * Mathf.Cos(Mathf.Deg2Rad * theta) - point.y * Mathf.Sin(theta * Mathf.Deg2Rad);
        //        //point.y = point.x * Mathf.Sin(Mathf.Deg2Rad * theta) + point.y * Mathf.Cos(theta * Mathf.Deg2Rad);

        //        Quaternion rotator = Quaternion.Euler(0, 0, theta);

        //        point.z = 0;
        //        point = rotator * point;
        //        curve.Add(point * scale);

        //    }
        //lineRenderer.positionCount = curve.Count;
        //lineRenderer.SetPositions(curve.ToArray());
        #endregion

        DrawCircle(circleInnerRadius,circleOuterRadius,factor);

    }
    [Header("Circle stuff")]
    public float circleInnerRadius;
    public float circleOuterRadius;
    [Range(0,100)]
    public int factor;
    public int factor2;
    public float deltaAngle;
    public int power;
    public void DrawCircle( float innerRadius, float outerRadius,float factor)
    {
        Vector3 point;
        List<Vector3> curve = new List<Vector3>();
        if (deltaAngle <= 0)
            deltaAngle = .1f;
        for (float i=0;i<360;i+=deltaAngle)
        {
            point.x = i;
            point.y = Mathf.Sin(i*Mathf.Deg2Rad) * (innerRadius+ outerRadius + outerRadius * powerSin(Mathf.Sin(i *Mathf.Deg2Rad*factor),power));
            point.z = 0;


            curve.Add(point);
        }
        lineRenderer.positionCount = curve.Count;
        lineRenderer.SetPositions(curve.ToArray());
    }

    float powerSin(float n, int power)
    {
        float x = n;
        for(int i=0;i<power;i++)
        {
            x = Mathf.Sin(x*factor2);
        }
        return x;
    }
}
