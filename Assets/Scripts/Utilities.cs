﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utilities
{
    static float? screenLeft = null;
    static float? screenRight = null;
    static float? screenTop = null;
    static float? screenBottom = null;
    static float? screenWidth = null;
    static float? screenHeight = null;
    static float? screenWidthInUnits = null;
    static float? screenHeightInUnits = null;

    public static float ScreenLeft
    {
        get
        {
            if (screenLeft == null)
                Initialize();
                return (float)screenLeft;
        }
    }
    public static float ScreenRight
    {
        get
        {
            if (screenRight == null)
                Initialize();
            return (float)screenRight;
        }
    }
    public static float ScreenTop
    {
        get
        {
            if (screenTop == null)
                Initialize();
            return (float)screenTop;
        }
    }
    public static float ScreenBottom
    {
        get
        {
            if (screenBottom == null)
                Initialize();
            return (float)screenBottom;
        }
    }
    public static float ScreenWidth
    {
        get
        {
            if (screenWidth == null)
                Initialize();
            return (float)screenWidth;
        }
    }
    public static float ScreenHeight
    {
        get
        {
            if (screenHeight == null)
                Initialize();
            return (float)screenHeight;
        }
    }
    public static float ScreenWidthInUnits
    {
        get
        {
            if (screenWidthInUnits == null)
                Initialize();
            return (float)screenWidthInUnits;
        }
    }

    public static float ScreenHeightInUnits
    {
        get
        {
            if (screenHeightInUnits == null)
                Initialize();
            return (float)screenHeightInUnits;
        }
    }


    static Utilities()
    {
        Initialize();
    }

    static void Initialize()
    {
        screenWidth = Screen.safeArea.width;
        screenHeight = Screen.safeArea.height;
        if (Camera.main)
        {
            screenWidthInUnits = Camera.main.ScreenToWorldPoint(Vector3.right * 0).x - Camera.main.ScreenToWorldPoint(Vector3.right * (float)screenWidth).x;
            screenHeightInUnits = Camera.main.ScreenToWorldPoint(Vector3.up * 0).y - Camera.main.ScreenToWorldPoint(Vector3.up * (float)screenHeight).y;
            screenLeft = -Mathf.Abs((float)screenWidthInUnits) / 2;
            screenRight = Mathf.Abs((float)screenWidthInUnits) / 2;
            screenTop = Mathf.Abs((float)screenHeightInUnits) / 2;
            screenBottom = -Mathf.Abs((float)screenHeightInUnits) / 2;
        }
    }
}
