﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimGuide : MonoBehaviour
{

    public GameObject Base;
    public LayerMask layerMask;
    public List<Vector3> pathPoints;
    public int numberOfReflections;

    public LineRenderer lineRenderer;

    static AimGuide instance;
    public static AimGuide Instance => instance;
    // Start is called before the first frame update
    void Start()
    {
        if(instance)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        pathPoints.Clear();
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);


        pathPoints.Add(Base.transform.position);
        GetReflection(Base.transform.position, mousePosition, numberOfReflections);
        lineRenderer.positionCount = pathPoints.Count;
        lineRenderer.SetPositions(pathPoints.ToArray());
    }

    public void GetReflection(Vector3 start, Vector3 end, int n)
    {
        RaycastHit2D raycastHit2D = Physics2D.Raycast(start, end - start, 10, layerMask.value);
        raycastHit2D = Physics2D.CircleCast(start, Grid.Instance.bubbleWidth / 3, end - start, 10, layerMask.value);
        if (raycastHit2D.collider && n > 0)
        {
            //print(raycastHit2D.point);
            pathPoints.Add(raycastHit2D.centroid);
            if (raycastHit2D.collider.gameObject.GetComponent<BubbleData>())
                return;
            GetReflection(raycastHit2D.centroid + Vector2.Reflect(end - start, raycastHit2D.normal).normalized * .01f,
                Vector2.Reflect(end - start, raycastHit2D.normal) + raycastHit2D.centroid,
                n - 1);
        }
        else
        {
            pathPoints.Add(start + (end - start).normalized * 10);
        }


    }

    #region OldCode

    //public GameObject basePosition;
    //public LineRenderer lineRenderer;
    //public int reflectionCount;
    //// Start is called before the first frame update
    //void Start()
    //{

    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);


    //    List<Vector3> points = new List<Vector3>();
    //    points.Add(basePosition.transform.position);
    //    points.Add(basePosition.transform.position + ((Vector3)mousePosition - base.transform.position).normalized);
    //    Debug.DrawLine(points[0],points[1]);

    //    RaycastHit2D raycastHit2D;// = Physics2D.Raycast(points[0], mousePosition - (Vector2)basePosition.transform.position,10);
    //    raycastHit2D = Physics2D.Raycast(points[0], points[1]-points[0], 100); ;
    //    for (int i = 1; i < reflectionCount+2; i++)
    //    {
    //        //print("i=" + i);
    //        //print("points.length=" + points.Count);
    //        /*if ((points.Count-1) < (i))
    //            continue;*/

    //        if (((RaycastHit2D)raycastHit2D).collider)
    //        {

    //            print("hit");
    //            print("Normal: " + raycastHit2D.normal);
    //            print(((RaycastHit2D)raycastHit2D).collider);
    //            //lineRenderer.SetPositions(new Vector3[] { basePosition.transform.position, raycastHit2D.point });
    //            points.Add(((RaycastHit2D)raycastHit2D).point);
    //            points.Add(Vector2.Reflect(points[i] - points[i - 1], (raycastHit2D).normal).normalized+ raycastHit2D.point);
    //            raycastHit2D = Physics2D.Raycast(points[i], Vector2.Reflect(points[i] - points[i - 1], (raycastHit2D).normal)+raycastHit2D.point, 10);
    //        }
    //        else
    //        {
    //            //points.Add(Vector2.Reflect(points[i ] - points[i - 1], ((RaycastHit2D)raycastHit2D).normal));
    //            //lineRenderer.SetPositions(new Vector3[] { basePosition.transform.position, basePosition.transform.position + ((Vector3)mousePosition - base.transform.position).normalized });
    //        }
    //    }
    //    lineRenderer.positionCount = points.Count;
    //    lineRenderer.SetPositions(points.ToArray());
//}
    #endregion
}
