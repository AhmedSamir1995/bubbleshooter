﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    int score;
    int shotsRemaining;
    
    public Text scoreText;
    public Text remainingShotsText;
    static GameManager instance;
    public static GameManager Instance =>instance;

    // Start is called before the first frame update
    void Start()
    {
        if(instance)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = value;
            scoreText.text = "Score\n" + score;
        }
    }

    public int ShotsRemaining
    {
        get
        {
            return shotsRemaining;
        }
        set
        {
            shotsRemaining = value;
            remainingShotsText.text = "Remaining shots\n" + shotsRemaining;
        }
    }

}
