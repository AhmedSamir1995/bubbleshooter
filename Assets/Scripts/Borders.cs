﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Borders : MonoBehaviour
{
    public float borderWidth;
    public GameObject leftBorder;
    public GameObject rightBorder;
    // Start is called before the first frame update
    void Start()
    {
        leftBorder.transform.localScale = new Vector3(borderWidth, 1000, 1);
        rightBorder.transform.localScale = new Vector3(borderWidth, 1000, 1);
        leftBorder.transform.position = new Vector3( Utilities.ScreenLeft-borderWidth/2, 0);
        rightBorder.transform.position = new Vector3(Utilities.ScreenRight+borderWidth/2, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
