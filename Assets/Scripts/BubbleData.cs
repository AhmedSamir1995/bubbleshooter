﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleData : MonoBehaviour
{
    public BubbleColor bubbleColor;
    public Vector2Int index;
    // Start is called before the first frame update
    void Start()
    {
        bubbleColor = (BubbleColor)Random.Range(1, 6);
        SetBubbleColor(bubbleColor);
        if(Grid.Instance)
        transform.localScale = Vector3.one * Grid.Instance.bubbleWidth;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetBubbleColor(BubbleColor bubbleColor)
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        switch (bubbleColor)
        {
            case BubbleColor.Red:
                spriteRenderer.color = Color.red;
                break;
            case BubbleColor.Green:
                spriteRenderer.color = Color.green;
                break;
            case BubbleColor.Blue:
                spriteRenderer.color = Color.blue;
                break;
            case BubbleColor.Yellow:
                spriteRenderer.color = Color.yellow;
                break;
            case BubbleColor.Magenta:
                spriteRenderer.color = Color.magenta;
                break;
            case BubbleColor.Cyan:
                spriteRenderer.color = Color.cyan;
                break;
            default:
                spriteRenderer.color = Color.white;
                break;
        }
    }
    private void OnMouseEnter()
    {
        /*SetBubbleColor((BubbleColor)Random.Range(1, 6));

        Vector2Int[] mySurounds = Grid.Instance.AdjacentBubblesIndeces(index);

        for (int i = 0; i < mySurounds.Length; i++)
        {
            GameObject temp = Grid.Instance[mySurounds[i].x, mySurounds[i].y];
            if(temp)
                temp.GetComponent<SpriteRenderer>().color = Color.red;



            //StartCoroutine(SetBubbleColor(Grid.Instance.Bubbles[mySurounds[i].y][mySurounds[i].x]));

        }*/
    }
    private void OnMouseExit()
    {
        /*SetBubbleColor((BubbleColor)0);


        SetBubbleColor((BubbleColor)Random.Range(1, 6));

        Vector2Int[] mySurounds = Grid.Instance.AdjacentBubblesIndeces(index);

        for (int i = 0; i < mySurounds.Length; i++)
        {


            Grid.Instance.SetBubbleColorImidiatly(Grid.Instance[mySurounds[i].x,mySurounds[i].y]);


            //StartCoroutine(SetBubbleColor(Grid.Instance.Bubbles[mySurounds[i].y][mySurounds[i].x]));

        }*/
    }
    public enum BubbleColor{
        White,
        Red,
        Green,
        Blue,
        Yellow,
        Magenta,
        Cyan
    }
    public enum BubbleType
    {
        Color,
        Score,
        Lives,
        Bomb,
        Fire,
        Wildcard
    }

    public void Pop()
    {
        print("Index"+index);
        GameManager.Instance.Score += 100;
        Destroy(gameObject);
    }
}
