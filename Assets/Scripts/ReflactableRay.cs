﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflactableRay : MonoBehaviour
{

    public GameObject target;
    public GameObject Base;
    public LayerMask layerMask;
    public List<Vector3> points;
    public int numberOfReflections;

    public LineRenderer lineRenderer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        points.Clear();
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        target.transform.position = mousePosition;
        points.Add(Base.transform.position);
        GetReflection(Base.transform.position, target.transform.position, numberOfReflections);
        lineRenderer.positionCount = points.Count;
        lineRenderer.SetPositions(points.ToArray());
    }

    public void GetReflection(Vector3 start, Vector3 end, int n)
    {
        RaycastHit2D raycastHit2D = Physics2D.Raycast(start, end-start, 10, layerMask.value);
        if (raycastHit2D.collider && n > 0)
        {
            //print(raycastHit2D.point);
            points.Add(raycastHit2D.point);
            if (raycastHit2D.collider.gameObject.GetComponent<BubbleData>())
                return;
            GetReflection(raycastHit2D.point + Vector2.Reflect(end - start, raycastHit2D.normal).normalized*.01f,
                Vector2.Reflect(end - start, raycastHit2D.normal) + raycastHit2D.point,
                n - 1);
        }
        else
        {
            points.Add(start + (end - start).normalized*10);
        }


    }
}
