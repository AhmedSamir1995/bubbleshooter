﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleShootable : Shootable
{
    public float speed;
    public float pathLength;
    public Vector3[] path;
    public float[] lengths;
    public int lastPointIndex;
    public float passedLength = 0;

    [ContextMenu("Shoot")]
    public override void Shoot()
    {
        path = AimGuide.Instance.pathPoints.ToArray();
        lengths = new float[path.Length];
        pathLength = GetPathLenth();
        lastPointIndex = 0;
        passedLength = 0;
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        GetComponent<Collider2D>().enabled = true;
        this.enabled = true;

    }

    private void Update()
    {
        Proceed();
    }

    float GetPathLenth()
    {
        float length=0;
        for(int i=0;i<path.Length-1;i++)
        {
            lengths[i] = Vector3.Distance(path[i], path[i + 1]);
            length += lengths[i];
        }
        return 0;
    }

    void Proceed()
    {
        if (lastPointIndex >= lengths.Length)
        {
            this.enabled = false;
            AddToGrid();
            return;
        }

        passedLength +=Time.deltaTime*speed;
        if(passedLength<lengths[lastPointIndex])
            transform.position= Vector3.Lerp(path[lastPointIndex], path[lastPointIndex+1], passedLength / lengths[lastPointIndex]);
        else
        {
            passedLength = 0;
            lastPointIndex++;
        }
    }
    void AddToGrid()
    {
        Vector2Int index = Grid.Instance.BubbleIndexFromPosition(transform.position, -1);
        transform.position = Grid.Instance.bubblePositionFromIndex(index.y, index.x, -1);
        GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        BubbleData bubbleData = this.gameObject.GetComponent<BubbleData>();
        bubbleData.index = index;
        Grid.Instance.AddBubble(bubbleData);
        Grid.Instance.CheckSurroundingBubblesColors(bubbleData, bubbleData.index, bubbleData.bubbleColor);
    }
}
