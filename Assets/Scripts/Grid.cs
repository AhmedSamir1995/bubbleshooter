﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public int bubblePerWidth;
    public int rowCount;
    public float screenWidth;
    public Vector3 screenSizeInUnits;
    public float startX;
    public float bubbleWidth;
    public BubbleData bubblePrefab;

    /// <summary>
    /// Note: This array is stored Y then X
    /// </summary>
    //GameObject[][] bubbles;
    List<BubbleData> bubbles;
    public GameObject this[int i, int j]
    {
        get
        {
            return GetBubbleByIndex(i, j);
        }
    }

    static Grid instance;
    public static Grid Instance => instance;
    // Start is called before the first frame update
    void Start()
    {
        //bubbles = new GameObject[rowCount][];
        bubbles = new List<BubbleData>();
        if(instance)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        screenWidth = Utilities.ScreenWidth;
        screenSizeInUnits = Vector3.right* Utilities.ScreenWidthInUnits+Vector3.up*Utilities.ScreenHeightInUnits;
        startX = Utilities.ScreenLeft;// -Mathf.Abs(unit.x) / 2;
        bubbleWidth = Mathf.Abs(screenSizeInUnits.x) / bubblePerWidth;
        /*for (int j=0;j<rowCount;j++)
        for (int i = 0; i < bubblePerWidth; i++)
        {
            GameObject temp = GameObject.Instantiate(bubblePrefab);
            temp.transform.localScale = Vector3.one * bubbleWidth;
            temp.transform.position = new Vector3(startX + (i + .5f) * bubbleWidth
                , 0, 0);
        }
        for (int i = 0; i < (bubblePerWidth-1); i++)
        {
            GameObject temp = GameObject.Instantiate(bubblePrefab);
            temp.transform.localScale = Vector3.one * bubbleWidth;
            temp.transform.position = new Vector3(startX + (i + 1f) * bubbleWidth
                , 0.86602540378443864676372317075294f*bubbleWidth, 0);
        }*/


        for (int j = 0; j < rowCount; j++)
        {
            //bubbles[j] = new GameObject[bubblePerWidth - 1 * (j % 2)];
            for (int i = 0; i < bubblePerWidth-1*(j%2); i++)
            {
                BubbleData temp = GameObject.Instantiate(bubblePrefab.gameObject).GetComponent<BubbleData>();
                temp.transform.localScale = Vector3.one * bubbleWidth;
                temp.name = "Bubble(" + j + ", " + i + ")";
                temp.index = new Vector2Int(i, j);
                temp.transform.position = bubblePositionFromIndex(j,i,-1);
                bubbles.Add(temp);
            }
        }

        SortBubbles();
        SetFixedJoints();
    }

    public void SortBubbles()
    {
        bubbles.Sort((A, B) =>
        {
            return A.index.x + A.index.y * 1000 > B.index.x + B.index.y * 1000 ? 1 : -1;
        }
      );
    }
    public void SetFixedJoints()
    {
        for(int i=0;i<bubbles.Count;i++)
        {
            GameObject[] adjacentBubbles = AdjacentBubbles(bubbles[i].index);
            for(int j=0;j<adjacentBubbles.Length;j++)
            {
                if(adjacentBubbles[j].GetComponent<BubbleData>().index.y==0)
                {
                    adjacentBubbles[j].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                }
                else
                {
                    FixedJoint2D fixedJoint2D = adjacentBubbles[j].AddComponent<FixedJoint2D>();
                    fixedJoint2D.connectedBody = bubbles[i].GetComponent<Rigidbody2D>();
                    adjacentBubbles[j].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    adjacentBubbles[j].GetComponent<Rigidbody2D>().gravityScale = 1;
                    //bubbles[i].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                }
            }
        }
    }
    public void AddBubble(BubbleData bubble)
    {
        bubbles.Add(bubble);
        SortBubbles();
    }
    // Update is called once per frame
    void Update()
    {
        //Vector2 mousePosition = Input.mousePosition;
        //mousePosition = ScreenToWorldPoint(mousePosition);
        //Vector2Int bubbleIndex = BubbleIndexFromPosition(mousePosition, -1);
        //print("Bubble: " +bubbleIndex);
        //Vector2Int[] surrounds = AdjacentBubblesIndeces(bubbleIndex);
        //foreach(Vector2Int x in surrounds)
        //{
            //if (x.y >= 0 && x.y < bubbles.Length)
            //{
                //print("Surround" + x + "max bubbles Y" + bubbles[x.y].Length);
                //if (x.x >= 0 && x.x < bubbles[x.y].Length)
                //{
                    //if (bubbles[x.y][x.x].GetComponent<SpriteRenderer>())
                    //{
                        //bubbles[x.y][x.x].GetComponent<SpriteRenderer>().color = Color.red;
                        //StartCoroutine(SetBubbleColor(bubbles[x.y][x.x]));
                    //}

                //}
            //}
        //}

    }
    public IEnumerator SetBubbleColor(GameObject bubble)
    {
        yield return new WaitForEndOfFrame();
        bubble.GetComponent<SpriteRenderer>().color = Color.white;
    }

    public void SetBubbleColorImidiatly(GameObject bubble)
    {

        bubble.GetComponent<SpriteRenderer>().color = Color.white;
    }
    public Vector2 bubblePositionFromIndex(int row, int column, int Ydirection)
    {
        return new Vector3(startX + (column + .5f + .5f * (row % 2)) * bubbleWidth
                    , Ydirection * row * 0.86602540378443864676372317075294f * bubbleWidth, 0);
    }

    public Vector2Int BubbleIndexFromPosition(Vector2 position, int Ydirection)
    {
        Vector2Int index = new Vector2Int();
        index.y = Mathf.RoundToInt(position.y / (Ydirection * 0.86602540378443864676372317075294f * bubbleWidth));

        index.x = Mathf.RoundToInt((position.x - startX) / bubbleWidth - .5f - .5f * (index.y % 2));

        //position.x = startX + (column + .5f + .5f * (index.y % 2)) * bubbleWidth
        return index;
    }

    public Vector2Int[] AdjacentBubblesIndeces(Vector2Int bubbleIndex)
    {
        List<Vector2Int> adjacentIndeces = new List<Vector2Int>();
        //if (bubbleIndex.y < rowCount - 1 && bubbleIndex.x < (bubblePerWidth - 1 % bubbleIndex.y))
        //{
        //    if(bubbleIndex.x)
        //}

        adjacentIndeces.Add(new Vector2Int(bubbleIndex.x + bubbleIndex.y % 2, bubbleIndex.y + 1));
        adjacentIndeces.Add(new Vector2Int(bubbleIndex.x - 1 + bubbleIndex.y % 2, bubbleIndex.y + 1));
        adjacentIndeces.Add(new Vector2Int(bubbleIndex.x + 1, bubbleIndex.y));

        adjacentIndeces.Add(new Vector2Int(bubbleIndex.x - 1, bubbleIndex.y));
        adjacentIndeces.Add(new Vector2Int(bubbleIndex.x + bubbleIndex.y % 2, bubbleIndex.y - 1));
        adjacentIndeces.Add(new Vector2Int(bubbleIndex.x - 1 + bubbleIndex.y % 2, bubbleIndex.y - 1));

        adjacentIndeces.RemoveAll(x =>
            !GetBubbleByIndex(x.x, x.y)
        );

        return adjacentIndeces.ToArray();

    }

    public GameObject[] AdjacentBubbles(Vector2Int bubbleIndex)
    {
        List<GameObject> adjacentBubbles = new List<GameObject>();

        Vector2Int[] adjecentBubblesIndeces = AdjacentBubblesIndeces(bubbleIndex);

        for (int i = 0; i < adjecentBubblesIndeces.Length; i++)
        {
            adjacentBubbles.Add(GetBubbleByIndex( adjecentBubblesIndeces[i].x,adjecentBubblesIndeces[i].y));
        }

        return adjacentBubbles.ToArray();
    }

    Vector3 ScreenToWorldPoint(Vector3 point)
    {
        return Camera.main.ScreenToWorldPoint(point);
    }

    public GameObject GetBubbleByIndex(int x, int y)
    {
        

        BubbleData temp = bubbles.Find((A) =>
        {
            return (A.index.x == x) && (A.index.y == y);
        });

        if(temp==null)
        {
            //print("(" + x + ", " + y + ") not found");
        } 

        //int leftBorder = 0, rightBorder = bubbles.Count - 1;
        //int mid = (leftBorder + rightBorder) / 2;
        //int i = 0;
        //while(leftBorder<rightBorder&&i<100000)
        //{
        //    print("Mid: " + mid + " Left: " + leftBorder + " Right: " + rightBorder);
        //    mid = (leftBorder + rightBorder) / 2;

        //    if ((bubbles[mid].index.x + bubbles[mid].index.y * 1000) == (x + y * 1000))
        //    {
        //        temp = bubbles[mid];
        //        break;
        //    }
        //    if ((bubbles[mid].index.x + bubbles[mid].index.y * 1000) > (x + y * 1000))
        //        rightBorder = mid;
        //    if ((bubbles[mid].index.x + bubbles[mid].index.y * 1000) < (x + y * 1000))
        //        leftBorder = mid;
        //    i++;
        //}
        if (temp)
            return temp.gameObject;
        return null;
    }


    public void CheckSurroundingBubblesColors(BubbleData bubbleData, Vector2Int index, BubbleData.BubbleColor bubbleColor)
    {
        GameObject[] surrounds = AdjacentBubbles(index);
        for(int i=0;i<surrounds.Length;i++)
        {
            BubbleData temp = surrounds[i].GetComponent<BubbleData>();

            if (temp && bubbleData)
            {
                if (temp.GetComponent<BubbleData>().index.y == 0)
                {
                    temp.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                }
                else
                {
                    FixedJoint2D fixedJoint2D = temp.gameObject.AddComponent<FixedJoint2D>();
                    fixedJoint2D.connectedBody = bubbleData.GetComponent<Rigidbody2D>();
                    temp.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    temp.GetComponent<Rigidbody2D>().gravityScale = 1;

                    FixedJoint2D fixedJoint2D2 = bubbleData.gameObject.AddComponent<FixedJoint2D>();
                    fixedJoint2D2.connectedBody = temp.GetComponent<Rigidbody2D>();
                    bubbleData.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    bubbleData.GetComponent<Rigidbody2D>().gravityScale = 1;
                    //bubbles[i].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                }
            }

            if (temp&&(temp.bubbleColor==bubbleColor))
            {

                print("" + temp.index + temp.name + temp.bubbleColor +", " + bubbleColor);
                StartCoroutine(CheckSurroundingBubblesColorsDelayed(temp, temp.index, bubbleColor));
                temp.Pop();
                //CheckSurroundingBubblesColors(temp, temp.index, bubbleColor);
                bubbles.Remove(bubbleData);
                if(bubbleData&&bubbleData.gameObject)
                    bubbleData.Pop();
            }
        }
    }

    IEnumerator CheckSurroundingBubblesColorsDelayed(BubbleData bubbleData, Vector2Int index, BubbleData.BubbleColor bubbleColor)
    {
        yield return new WaitForSeconds(.1f);
        CheckSurroundingBubblesColors(bubbleData, index, bubbleColor);
    }
}
