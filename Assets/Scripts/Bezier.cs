﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bezier : MonoBehaviour
{

    public List<GameObject> pointsIn;
    [Range(0.001f,1)]
    public float deltaTime;

    public List<Vector3> curvePoints;



    public LineRenderer lineRenderer;
    // Start is called before the first frame update
    void Start()
    {

        curvePoints.Clear();
        for (float t = 0; t <= 1.1f; t += deltaTime)
        {
            if (t > 1)
                t = 1;
            curvePoints.Add(B(t));
            print("T:" + t);
            if (t == 1)
                break;
        }

        lineRenderer.positionCount = curvePoints.Count;
        lineRenderer.SetPositions(curvePoints.ToArray());
    }

    Vector3 curveHead;
    Vector3 curveDirection;
    // Update is called once per frame
    void Update()
    {


        
        //for (int i = 0; i < (curvePoints.Count - 1); i++)
        //{
        //    Debug.DrawLine(curvePoints[i], curvePoints[i + 1], Color.red);
        //}


    }
    void DrawCurveFor(Vector3 startPoint,Vector3 endPoint, float endT)
    {
        //Vector3 basePoint = curveHead;
        //float t=0;

        //Vector3 currentPosition= Vector3.Lerp(startPoint,endPoint,1-t);
        //for(int i=0;t<=endT;i++)
        //{
        //    currentPosition = basePoint + (t) * (endPoint - startPoint);// Vector3.Lerp(startPoint, endPoint,t);




        //    //currentPosition = Vector3.Lerp(startPoint, endPoint, t);
        //    t += deltaTime;
        //    curveHead = currentPosition;//Vector3.Lerp(curveHead, currentPosition, t);
        //    curvePoints.Add(curveHead);
        //}

    }



    int BinomialCoefficient(int A, int B)
    {
        /// / n \     n!
        /// |   | = ----------
        /// \ i /    i! * (n-i)!
        /// 

        return Factorial(A) / (Factorial(B) * Factorial(A - B));
    }


    int Factorial(int n)
    {
        if (n < 0)
            return -1;
        if (n == 0)
            return 1;
        return n*Factorial(n - 1);
    }


    Vector3 B(float T)
    {
        Vector3 result= Vector3.zero;
        int n = pointsIn.Count-1;

        for(int i=0;i<=n;i++)
        {

            result += BinomialCoefficient(n, i) * Power(1 - T, n - i) * Power(T, i) * (pointsIn[i].transform.position- pointsIn[0].transform.position);

            //Power((1 - T), 5) * pointsIn[i].transform.position
        }

        //Power((1-5),5)*pointsIn[0] + 5t

        return result+ pointsIn[0].transform.position;
    }

    float Power(float n, int e)
    {
        float result = 1;
        for(int i=0;i<e;i++)
        {
            result = result * n;
        }
        return result;
    }

    private void OnDrawGizmos()
    {
        for(int i=0;i<pointsIn.Count;i++)
        {
                Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(pointsIn[i].transform.position, .1f);
            if(i<pointsIn.Count-1)
            {
                Gizmos.color = Color.red;

                Gizmos.DrawLine(pointsIn[i].transform.position, pointsIn[i + 1].transform.position);
                //Debug.DrawLine(pointsIn[i].transform.position, pointsIn[i + 1].transform.position);
            }
        }



    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;

        for (int i = 0; i < pointsIn.Count; i++)
        {
            Gizmos.DrawWireSphere(pointsIn[i].transform.position, .1f);
            if (i < pointsIn.Count - 1)
            {

                Gizmos.DrawLine(pointsIn[i].transform.position, pointsIn[i + 1].transform.position);
                //Debug.DrawLine(pointsIn[i].transform.position, pointsIn[i + 1].transform.position);
            }
        }
    }
}
